# Small Test Project

# Setup
    - create Virtual env and activate it
    - pip install req.txt
	

# New Features!
	- Custom login page added only for writers. */login
	- Add custom permission "Is writer"
	- Add Multiple Custom Views and form.
	
# Requirements fullfills:

	- Writer can update articles that assigned him.
	- Writer can assign the articles that not assigned yet.
	- Writer can add new articles


# simply visit 127.0.0.1:8000 and get on login page:

Login SuperAdmin: 
UserName: adnan
Password: 123

Login with writer permission:
Login: test
password: test

