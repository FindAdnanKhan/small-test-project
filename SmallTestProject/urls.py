"""SmallTestProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from content.admin import writer_admin_site
from django.urls import path
from content import views

urlpatterns = [
    path('', views.Dashboard, name='dashboard'),
    path('logout/', views.Logout, name='logout'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('open_article/', views.open_article, name='open_article'),
    path('add_article/', views.add_article, name='add_article'),
    path('edit_article/<int:pk>', views.edit_article, name='edit_article'),
    path('assign_article/<int:pk>', views.assign_article, name='assign_article'),
    path('assign_confirm/<int:pk>/<int:dec>', views.assign_confirm, name='assign_confirm'),
    path('admin/', admin.site.urls),
    path('content_admin/', writer_admin_site.urls),
]
