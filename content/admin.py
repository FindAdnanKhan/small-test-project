from django.contrib.admin import AdminSite
from django.contrib import admin
from content.models import Article, User
from django.db.models import Q
# Register your models here.

admin.site.register(Article)
admin.site.register(User)


class WriterAdminSite(AdminSite): # Custom Admin section for writer
    site_header = "Article Control Panel"
    site_title = "Writer Custom Control Panel"
    index_title = "Welcome to Writer Control Panel"


class ArticleDisplay(admin.ModelAdmin):
    list_display = ['title', 'status']

    def get_queryset(self, request):
        qs = super(ArticleDisplay, self).get_queryset(request)
        return qs.filter(Q(editor=request.user) | Q(editor=None))


writer_admin_site = WriterAdminSite(name='writer_admin')


writer_admin_site.register(Article, ArticleDisplay)
