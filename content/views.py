from django.shortcuts import render, redirect
from django.views.generic.edit import FormView
from . import forms
from django.contrib.auth import login, authenticate, logout
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from content.models import Article


@permission_required("content.is_writer", login_url='/login/')
def Dashboard(request):
    context = dict()
    context['articles'] = Article.objects.filter(editor=request.user)
    return render(request, 'dashboard.html', context)


def Logout(request):
    logout(request)
    return HttpResponseRedirect(reverse_lazy('dashboard'))


@permission_required("content.is_writer", login_url='/login/')
def open_article(request):
    context = dict()
    context['articles'] = Article.objects.filter(editor=None)
    context['assign'] = 1
    return render(request, 'dashboard.html', context)


@permission_required("content.is_writer", login_url='/login/')
def assign_article(request, pk):
    article = Article.objects.get(pk=pk)
    if article.editor is not None:
        return HttpResponseRedirect(reverse_lazy('dashboard'))
    context = dict()
    context["id"] = pk
    return render(request, 'sure.html', context)


@permission_required("content.is_writer", login_url='/login/')
def assign_confirm(request, pk, dec):
    article = Article.objects.get(pk=pk)
    if article.editor is not None or dec != 1:
        return HttpResponseRedirect(reverse_lazy('dashboard'))
    article.editor = request.user
    article.save()
    return HttpResponseRedirect(reverse_lazy('dashboard'))


class LoginView(FormView):
    form_class = forms.LoginForm
    success_url = reverse_lazy('dashboard')
    template_name = 'login.html'

    def form_valid(self, form):
        """ process user login"""
        credentials = form.cleaned_data

        user = authenticate(username=credentials['username'],
                            password=credentials['password'])

        if user is not None and user.has_perm('content.is_writer'):
            login(self.request, user)
            return HttpResponseRedirect(self.success_url)
        else:
            messages.add_message(self.request, messages.INFO, 
                'Wrong credentials Or not Allowed please try again')
            return HttpResponseRedirect(reverse_lazy('login'))


@permission_required("content.is_writer", login_url='/login/')
def edit_article(request, pk):
    article = Article.objects.get(pk=pk)
    article_form = forms.ArticleForm(instance=article)
    if request.user != article.editor and request.user.has_perm('content.is_writer'):
        return HttpResponseRedirect(reverse_lazy('dashboard'))
    if request.method == 'POST':
        form_data = forms.ArticleForm(request.POST, instance=article)
        form_data.save()
        return redirect('dashboard')
    return render(request, 'forms.html', context={'form':article_form})


@permission_required("content.is_writer", login_url='/login/')
def add_article(request):
    article_form = forms.ArticleForm()
    if request.method == 'POST':
        form_data = forms.ArticleForm(request.POST)
        form_data =form_data.save(commit=False)
        form_data.editor = request.user
        form_data.save()
        return redirect('dashboard')
    return render(request, 'forms.html', context={'form':article_form})
