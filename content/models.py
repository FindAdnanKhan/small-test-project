from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime
from django.conf import settings

# Create your models here.


class User(AbstractUser):
    class Meta:
        permissions = (
            ("is_writer", "Is Writer"),
        )


class Article(models.Model):
    title = models.CharField(max_length=20)
    editor = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    date = models.DateTimeField(default=datetime.now())
    content = models.TextField()

    STATUS_CHOICES = (
        (1, 'Draft'),
        (2, 'Open'),
        (3, 'Review'),
        (4, 'Approved'),
        (5, 'Complete'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
