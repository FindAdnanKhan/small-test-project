from django import forms
from django.contrib.auth import get_user_model
from content.models import Article


class LoginForm(forms.Form):
    """user login form"""
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        exclude = ('editor', )
